# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 14:19:12 2017

@author: sl5412
"""
from get_dff_with_stim import get_dff_stim
from scipy.signal import resample
import numpy as np
import scipy.io as sio
from scipy.misc import imresize


# Prepare stim for MNE
def MNE_preprocess(stimulus, response, stim_compress = 1, resample_option = 'up'):

    if (stim_compress != 1):
        stim_compressed = np.asarray([imresize(frame,stim_compress) for frame in stimulus[:,:,:]])
    else:
        stim_compressed = stimulus
        
    stim = np.asarray(stim_compressed)
    
    stim_len = float(stim.shape[2])
    _, response_len = response.shape
    
    
    if (resample_option == 'up'):
        
        #Upsample stim matrix
        stim_resampled = resample(stim,response_len)
        sio.savemat('stimulus',mdict={'stimulus': stim_resampled})
        sio.savemat('response',mdict={'response': response})
        np.save('stimulus.npy',stim_resampled)
        np.save('response.npy',response)

        
    elif (resample_option == 'down'):
        #Downsample response signal
        response_resampled = resample(response.T, int(stim_len)).T
        sio.savemat('stimulus',mdict={'stimulus': stim})
        sio.savemat('response',mdict={'response': response_resampled})
        np.save('stimulus.npy', stim)
        np.save('response.npy', response_resampled)
    

    

