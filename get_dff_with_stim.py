# -*- coding: utf-8 -*-
def get_dff_stim():
    from allensdk.core.brain_observatory_cache import BrainObservatoryCache
    
    import pprint
    
    
    
    # This class uses a 'manifest' to keep track of downloaded data and metadata.  
    # All downloaded files will be stored relative to the directory holding the manifest
    # file.  If 'manifest_file' is a relative path (as it is below), it will be 
    # saved relative to your working directory.  It can also be an absolute path.
    boc = BrainObservatoryCache(manifest_file='boc/manifest.json')
    
    # Download a list of all targeted areas
    targeted_structures = boc.get_all_targeted_structures()
    
    
    # Download experiment containers for VISp experiments
    visp_ecs = boc.get_experiment_containers(targeted_structures=['VISp'])
    
    
    # Download a list of all imaging depths
    depths = boc.get_all_imaging_depths()
    
    
    
    
    # Download a list of all stimuli
    stims = boc.get_all_stimuli()

    
    
    # Download a list of all cre driver lines 
    cre_lines = boc.get_all_cre_lines()

    
    # Download experiment containers for Cux2 experiments
    cux2_ecs = boc.get_experiment_containers(cre_lines=['Cux2-CreERT2'])
    

    
    # Find all of the experiments for an experiment container
    cux2_ec_id = cux2_ecs[0]['id']
    exps = boc.get_ophys_experiments(experiment_container_ids=[cux2_ec_id])

    
    
    import allensdk.brain_observatory.stimulus_info as stim_info
    
    # Find the experiment with the static static gratings stimulus
    cux2_ec_id = cux2_ecs[0]['id']
    exp = boc.get_ophys_experiments(experiment_container_ids=[cux2_ec_id], 
                                    stimuli=[stim_info.STATIC_GRATINGS])[0]

    
    exp = boc.get_ophys_experiment_data(exp['id'])
    
    # print out the metadata available in the NWB file
    
    
    
    import pandas as pd
    
    # Download cells for a set of experiments and convert to DataFrame
    cells = boc.get_cell_specimens()
    cells = pd.DataFrame.from_records(cells)
    # find direction selective cells in VISp
    visp_ec_ids = [ ec['id'] for ec in visp_ecs ]
    visp_cells = cells[cells['experiment_container_id'].isin(visp_ec_ids)]
    
    # significant response to static gratings stimulus
    sig_cells = visp_cells[visp_cells['p_dg'] < 0.005]
    
    # direction selective cells
    dsi_cells = sig_cells[(sig_cells['osi_dg'] > 0.5) & (sig_cells['osi_dg'] < 1.5)]
    
    
    import allensdk.brain_observatory.stimulus_info as stim_info
    
    # find experiment containers for those cells
    dsi_ec_ids = dsi_cells['experiment_container_id'].unique()
    
    # Download the ophys experiments containing the drifting gratings stimulus for VISp experiment containers
    dsi_exps = boc.get_ophys_experiments(experiment_container_ids=dsi_ec_ids, stimuli=[stim_info.DRIFTING_GRATINGS])

    
    
    # pick a direction-selective cell and find its NWB file
    dsi_cell = dsi_cells.iloc[345]
    
    # figure out which ophys experiment has the natural movie one stimulus for the cell's experiment container
    cell_exp = boc.get_ophys_experiments(experiment_container_ids=[dsi_cell['experiment_container_id']], 
                                         stimuli=[stim_info.DRIFTING_GRATINGS])[0]
    
    data_set = boc.get_ophys_experiment_data(cell_exp['id'])

    
    dsi_cell_id = dsi_cell['cell_specimen_id']
    time, raw_traces = data_set.get_fluorescence_traces(cell_specimen_ids=[dsi_cell_id])
    _, neuropil_traces = data_set.get_neuropil_traces(cell_specimen_ids=[dsi_cell_id])
    _, corrected_traces = data_set.get_corrected_fluorescence_traces(cell_specimen_ids=[dsi_cell_id])
    _, dff_traces = data_set.get_dff_traces(cell_specimen_ids=[dsi_cell_id])
    
    stim = data_set.get_stimulus_template(stim_info.DRIFTING_GRATINGS)
    
    return (dff_traces[0], stim)

