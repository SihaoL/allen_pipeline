# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 18:06:44 2017

@author: sl5412
"""
import numpy as np

def unravel_fluo_sig(spike_train,num_frames,num_trials):
    spike_train_matrix = np.reshape(spike_train, (num_trials, num_frames))
    counts = np.asarray([sum(time_bin) for time_bin in spike_train_matrix.T])/num_trials
    return counts