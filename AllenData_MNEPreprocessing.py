# -*- coding: utf-8 -*-
"""
Created on Wed Mar 08 11:16:00 2017

Get NWB File, extract stim_table for static gratings.
Generate static gratings based on stim_table and store them in matrix.

@author: sl5412
"""

from allensdk.core.brain_observatory_cache import BrainObservatoryCache
from writeGratingStack import writeGratingStack
import numpy as np
import matplotlib.pyplot as plt
from prepare_allendata import MNE_preprocess

boc = BrainObservatoryCache(manifest_file='boc/manifest.json')

filename =501498760#	501498760
data_set = boc.get_ophys_experiment_data(filename)


stim_table = data_set.get_stimulus_table('static_gratings')
fluo_traces = data_set.get_fluorescence_traces()[1]

# Extract part of the fluorescence trace during which static gratings where
# presented

start_times = np.array(stim_table.start)[:-1]
end_times = np.array(stim_table.end)[1:]
check_value_change = start_times < end_times-60
cut_offs = np.where(check_value_change)

stim_block_1_end = [stim_table.end[cut_offs[0][0]]]
stim_block_2_start = [stim_table.start[cut_offs[0][0]+1]]

stim_block_2_end = [stim_table.end[cut_offs[0][1]]]
stim_block_3_start = [stim_table.start[cut_offs[0][1]+1]]

indices_1 = np.arange(start_times[0],stim_block_1_end[0]+1)
indices_2 = np.arange(stim_block_2_start[0],stim_block_2_end[0]+1)
indices_3 = np.arange(stim_block_3_start[0], end_times[-1]+1)
indices_static_gratings = np.hstack((indices_1, indices_2, indices_3))
static_grating_response_trace = fluo_traces[:,indices_static_gratings]

try:
    gratingStack = np.load(str(filename)+'_gratingStack_triple.npy')
except IOError:
    gratingStack = writeGratingStack(stim_table)

MNE_preprocess(gratingStack, static_grating_response_trace, resample_option = 'down')