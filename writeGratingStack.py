# -*- coding: utf-8 -*-
"""
Created on Mon Mar 06 13:55:50 2017

Extracts stimulus table from a NWB file with static gratings and uses PsychoPy 
to write image stacks (numpy arrays) with the grating parameters defined in the stimulus
table.

@author: sl5412
"""
from psychopy import visual, core, monitors
import numpy as np
from tqdm import tqdm

def writeGratingStack(stim_table, contrast = 0.8, size=(80,128)):
    mon = monitors.Monitor('Test')
    win = visual.Window(
    size=(size[0]/2,size[1]/2),
    units="pix",
    fullscr=False,
    monitor=mon
    )

    frame = np.array([]).reshape(size[0]/2,size[1]/2,0)
    for index, row in tqdm(stim_table.iterrows()):

        try:
            grating = visual.GratingStim(
                win=win,
                units="pix",
                contrast = contrast,
                size=size,
                ori = row.orientation,
                phase = row.phase,
                sf = row.spatial_frequency
            )
            grating.draw()

        except ValueError:
                rect = visual.Rect(
                    win = win,
                    width = size[0],
                    height = size[1],
                    color = 'gray'
                )
                rect.draw()
                
        frame_from_buffer = np.array(win._getFrame(buffer='back'))[:,:,0].T
        frame = np.dstack((frame,frame_from_buffer)) #Have each frame thrice
        frame = np.dstack((frame,frame_from_buffer))
        frame = np.dstack((frame,frame_from_buffer))
    
    win.close()
    return frame